
package com.burchyk.forteknowledgeexam;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.burchyk.forteknowledgeexam.entity.Vacancy;

public class MainActivity extends Activity {

    public static final String DEBUG_TAG = "VacancyDebug";
    Button btnGetJSON = null;
    private VacancyAdapter vacancyAdapter;
    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnGetJSON = (Button) findViewById(R.id.btnGetJson);
        list = (ListView) findViewById(R.id.list);
        vacancyAdapter = new VacancyAdapter(new ArrayList<Vacancy>(), this);
        list.setAdapter(vacancyAdapter);
        list.setOnItemClickListener(new VacancyItemClickListener());
        btnGetJSON.setOnClickListener(new GetJsonOnClickListener(vacancyAdapter, list));
    }

    private static class GetJsonOnClickListener implements View.OnClickListener {

        private static String vacancyUrl = "https://api.hh.ru/vacancies/";
        private VacancyAdapter vacancyAdapter;
        private ListView list;

        public GetJsonOnClickListener(VacancyAdapter vacancyAdapter, ListView list) {
            this.vacancyAdapter = vacancyAdapter;
            this.list = list;
        }

        @Override
        public void onClick(View v) {
            Context context = v.getContext();
            if (checkInternetAccess(context)) {
                getJsonData(context);
                Log.d(DEBUG_TAG, "end in click");
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.exc_connection),
                        Toast.LENGTH_LONG).show();
            }

        }

        private void getJsonData(Context context) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new DownloadJSONTask(vacancyAdapter).execute(vacancyUrl);
                Log.d(DEBUG_TAG, "end in getJSON");
            }
        }

        private boolean checkInternetAccess(Context context) {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            } else {
                Log.e(DEBUG_TAG, context.getResources().getString(R.string.exc_connection));
                return false;
            }
        }
    }

    private static class VacancyItemClickListener implements OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Toast.makeText(view.getContext(), "From onItemClickListener", Toast.LENGTH_LONG).show();
        }
    }

}
