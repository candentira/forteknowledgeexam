
package com.burchyk.forteknowledgeexam;

import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.burchyk.forteknowledgeexam.entity.Employer;
import com.burchyk.forteknowledgeexam.entity.Vacancy;

public class VacancyAdapter extends ArrayAdapter<Vacancy> {

    private Context context;
    private List<Vacancy> vacancies;

    public VacancyAdapter(List<Vacancy> vacancies, Context context) {
        super(context, android.R.layout.simple_list_item_1, vacancies);
        this.context = context;
        this.vacancies = vacancies;
    }

    @Override
    public int getCount() {
        if (vacancies != null) {
            return vacancies.size();
        }
        return 0;
    }

    @Override
    public Vacancy getItem(int position) {
        if (vacancies != null) {
            return vacancies.get(position);
        }
        return null;
    }

    public long getItemId(int position) {
        if (vacancies != null) {
            return Long.parseLong(vacancies.get(position).getId());
        }
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            TextView vacancyName = (TextView) convertView.findViewById(R.id.vacancyName);
            TextView vacancyPublishTime = (TextView) convertView
                    .findViewById(R.id.vacancyPublishTime);
            ImageView vacancyLogo = (ImageView) convertView.findViewById(R.id.vacancyLogo);
            TextView vacancyPublisher = (TextView) convertView.findViewById(R.id.vacancyPublisher);

            ViewHolder holder = new ViewHolder(vacancyName, vacancyPublishTime, vacancyLogo,
                    vacancyPublisher);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.vacancyName.setText(vacancies.get(position).getName());
        holder.vacancyPublishTime.setText(vacancies.get(position).getPublished_at());
        Employer employer = vacancies.get(position).getEmployer();
        if (employer != null) {
            if (employer.getName() != null) {
                holder.vacancyPublisher.setText(employer.getName());
            }
            if (employer.getLogo_urls() != null && employer.getLogo_urls().getOriginal() != null) {
                holder.vacancyLogo.setImageURI(Uri.parse(vacancies.get(position).getEmployer()
                        .getLogo_urls().getOriginal()));
            }
        }
        return convertView;
    }

    private class ViewHolder {
        public final TextView vacancyName;
        public final TextView vacancyPublishTime;
        public final ImageView vacancyLogo;
        public final TextView vacancyPublisher;

        public ViewHolder(TextView vacancyName, TextView vacancyPublishTime, ImageView vacancyLogo,
                TextView vacancyPublisher) {
            super();
            this.vacancyName = vacancyName;
            this.vacancyPublishTime = vacancyPublishTime;
            this.vacancyLogo = vacancyLogo;
            this.vacancyPublisher = vacancyPublisher;
        }

    }

    public List<Vacancy> getItemList() {
        return vacancies;
    }

    public void setItemList(List<Vacancy> vacancies) {
        this.vacancies = vacancies;
    }

}
