
package com.burchyk.forteknowledgeexam;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.burchyk.forteknowledgeexam.entity.Employer;
import com.burchyk.forteknowledgeexam.entity.Logo_urls;
import com.burchyk.forteknowledgeexam.entity.Vacancy;

public class JSONVacancyParser {

    /**
     * Parsing Big JSON object, getting Vacancies from it it would be much nicer
     * to use Gson;)
     * 
     * @param strJSON
     * @return
     * @throws JSONException
     */
    public static List<Vacancy> parseJSONVacancyShort(String strJSON) throws JSONException {
        JSONObject json = new JSONObject(strJSON);
        JSONArray jItemsArray = json.getJSONArray(JSONVacancyConst.ITEMS);
        List<Vacancy> resultList = new ArrayList<Vacancy>();
        for (int i = 0; i < jItemsArray.length(); i++) {
            JSONObject jVacancy = jItemsArray.getJSONObject(i);
            Vacancy vacancy = new Vacancy();
            vacancy.setName(jVacancy.getString(JSONVacancyConst.NAME));
            vacancy.setPublished_at(jVacancy.getString(JSONVacancyConst.PUBLISHED_AT));
            if (!jVacancy.isNull(JSONVacancyConst.ID)) {
                vacancy.setId(jVacancy.getString(JSONVacancyConst.ID));
            }
            if (!jVacancy.isNull(JSONVacancyConst.EMPLOYER)) {
                Employer employer = new Employer();
                JSONObject jEmployer = jVacancy.getJSONObject(JSONVacancyConst.EMPLOYER);
                employer.setName(jEmployer.getString(JSONVacancyConst.EMPLOYER_NAME));
                if (!jEmployer.isNull(JSONVacancyConst.EMPLOYER_LOGO_URLS)) {
                    Logo_urls logo_urls = new Logo_urls();
                    JSONObject jLogoUrls = jEmployer.getJSONObject(JSONVacancyConst.EMPLOYER_LOGO_URLS);
                    logo_urls.set90(jLogoUrls.getString(JSONVacancyConst.EMPLOYER_LOGO_URLS_90));
                    logo_urls.set240(jLogoUrls.getString(JSONVacancyConst.EMPLOYER_LOGO_URLS_240));
                    logo_urls.setOriginal(jLogoUrls
                            .getString(JSONVacancyConst.EMPLOYER_LOGO_URLS_original));
                    employer.setLogo_urls(logo_urls);
                }
                vacancy.setEmployer(employer);
            }
            resultList.add(vacancy);
        }
        return resultList;
    }

}
