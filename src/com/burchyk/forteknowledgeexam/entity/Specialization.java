
package com.burchyk.forteknowledgeexam.entity;

public class Specialization {

    private String profarea_id;

    private String profarea_name;

    private String id;

    private String name;

    public String getProfarea_id() {
        return profarea_id;
    }

    public void setProfarea_id(String profarea_id) {
        this.profarea_id = profarea_id;
    }

    public String getProfarea_name() {
        return profarea_name;
    }

    public void setProfarea_name(String profarea_name) {
        this.profarea_name = profarea_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
