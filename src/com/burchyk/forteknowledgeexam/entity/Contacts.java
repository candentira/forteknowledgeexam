
package com.burchyk.forteknowledgeexam.entity;

import java.util.ArrayList;
import java.util.List;

public class Contacts {

    private String name;

    private String email;

    private List<Phone> phones = new ArrayList<Phone>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

}
