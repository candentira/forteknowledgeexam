
package com.burchyk.forteknowledgeexam;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.JSONException;

import android.os.AsyncTask;
import android.util.Log;

import com.burchyk.forteknowledgeexam.entity.Vacancy;

public class DownloadJSONTask extends AsyncTask<String, Void, Void> {

    private VacancyAdapter vacancyAdapter;
    private List<Vacancy> vacancy;

    public DownloadJSONTask(VacancyAdapter vacancyAdapter) {
        this.vacancyAdapter = vacancyAdapter;
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            String strJSON = downloadJSON(params[0]);
            vacancy = JSONVacancyParser.parseJSONVacancyShort(strJSON);
        } catch (IOException e) {
            e.printStackTrace();
            String strException = "Unable to retrieve web page. URL may be invalid.";
            Log.e(MainActivity.DEBUG_TAG, strException);
        } catch (JSONException e) {
            e.printStackTrace();
            String strException = "Couldn't parse JSON document";
            Log.e(MainActivity.DEBUG_TAG, strException);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (vacancy != null && vacancy.size() > 0) {
            vacancyAdapter.setItemList(vacancy);
            vacancyAdapter.notifyDataSetChanged();
        }
    }

    private String downloadJSON(String myurl) throws IOException {
        InputStream is = null;
        String strJSON;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // conn.setReadTimeout(10000);
            // conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(MainActivity.DEBUG_TAG, "The response is: " + response);
            strJSON = convertInputStreamToStr(conn.getInputStream());

        } finally {
            if (is != null) {
                is.close();
            }
        }
        return strJSON;
    }

    private String convertInputStreamToStr(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
