
package com.burchyk.forteknowledgeexam;

public class JSONVacancyConst {

    public static String ITEMS = "items";
    public static String DESCRIPTION = "description";
    public static String SCHEDULE = "schedule";
    public static String SCHEDULE_ID = "id";
    public static String SCHEDULE_NAME = "name";
    public static String ACCEPT_HANDICAPPED = "accept_handicapped";
    public static String EXPERIENCE = "experience";
    public static String EXPERIENCE_ID = "id";
    public static String EXPERIENCE_NAME = "name";
    public static String ADDRESS = "address";
    public static String ALTERNATE_URL = "alternate_url";
    public static String EMPLOYMENT = "employment";
    public static String EMPLOYMENT_ID = "id";
    public static String EMPLOYMENT_NAME = "name";
    public static String ID = "id";
    public static String SALARY = "salary";
    public static String SALARY_TO = "to";
    public static String SALARY_FROM = "from";
    public static String SALARY_CURRENCY = "currency";
    public static String ARCHIVED = "archived";
    public static String NAME = "name";
    public static String AREA = "area";
    public static String AREA_URL = "url";
    public static String AREA_ID = "id";
    public static String AREA_NAME = "name";
    public static String PUBLISHED_AT = "published_at";
    public static String RELATIONS = "relations";
    public static String NEGOTIATIONS_URL = "negotiations_url";
    public static String EMPLOYER = "employer";
    public static String EMPLOYER_LOGO_URLS = "logo_urls";
    public static String EMPLOYER_LOGO_URLS_90 = "90";
    public static String EMPLOYER_LOGO_URLS_240 = "240";
    public static String EMPLOYER_LOGO_URLS_original = "original";
    public static String EMPLOYER_NAME = "name";
    public static String EMPLOYER_URL = "url";
    public static String EMPLOYER_ALTERNATE_URL = "alternate_url";
    public static String EMPLOYER_ID = "id";
    public static String EMPLOYER_TRUSTED = "trusted";
    public static String RESPONSE_LETTER_REQUIRES = "response_letter_required";
    public static String RESPONSE_URL = "response_url";
    public static String TYPE = "type";
    public static String TYPE_ID = "id";
    public static String TYPE_NAME = "name";
    public static String TEST = "test";
    public static String TEST_REQUIRED = "required";
    public static String SPECIALIZATIONS_ARRAY = "specializations";
    public static String SPECIALIZATIONS_ARRAY_PROFAREA_ID = "profarea_id";
    public static String SPECIALIZATIONS_ARRAY_PROFAREA_NAME = "profarea_name";
    public static String SPECIALIZATIONS_ARRAY_ID = "id";
    public static String SPECIALIZATIONS_ARRAY_NAME = "name";
    public static String CONTACTS = "contacts";
    public static String CONTACTS_NAME = "name";
    public static String CONTACTS_EMAIL = "email";
    public static String CONTACTS_PHONES_ARAY = "phones";
    public static String CONTACTS_PHONES_ARAY_COMMENT = "comment";
    public static String CONTACTS_PHONES_ARAY_CITY = "city";
    public static String CONTACTS_PHONES_ARAY_NUMBER = "number";
    public static String CONTACTS_PHONES_ARAY_COUNTRY = "country";

}
